webtp3/docker
============================

TYPO3 docker testing image - This image is part of an automated testing enviroment.
Webservice can be linked to MySQL. More about the automated testing 
https://bitbucket.org/web-tp3/tp3_installer


Usage (standalone)
------------------

This image needs an external MySQL server or linked MySQL container. To create a MySQL container:

    docker run -d -e MYSQL_ROOT_PASSWORD="my-secret-pw" --name db -p 3306:3306 webtp3/tp3sql

To run TYPO3 by linking to the database created above:

    docker run -d --link db:db -e DB_PASS="<your_password>" -p 80:80 webtp3/docker

Usage (combined)
------------------
ia docker stack deploy or docker-compose

    docker-compose -f docker-compose.yml up

Now, you can use your web browser to access TYPO3 from the the follow address:

    http://localhost/typo3

User is "tp3min" and password is "Init1111".

    
The mysql Adminer 

    http://localhost:8080

Mysql Server can also be reached from outside on port 3306

SSH Server also for Rsync and other stuff
to use ssh server there is a user typo3user that needs a password (docker exec...)
graphicsmagick, imagemagick, letscrypt, openssl, php-xdebug and I think all what you need.

###start the server

You can easyly create your ohne Docker file based on this installation and deploy your app to it
Create a Dockerfile like the following:

    
    FROM webtp3/docker:18.4-stable
    
    RUN apt-get update && apt-get install -yq
    ADD . /var/www/html
    # start composer install
    RUN composer config  repositories.local path 'Packages/*' -d  /var/www/html/
    RUN  omposer --dev update -d  /var/www/html/
    
    # Expose environment variables for automated setup
    ENV DB_HOST **LinkMe**
    ENV DB_PORT **LinkMe**
    ENV DB_NAME typo3
    ENV DB_USER root
    ENV DB_PASS my-secret-pw
    ENV INSTALL_TOOL_PASSWORD password
    
    #serverroot is in /var/www/html/web we use the vendor dir for that
    # for productive enviroment we recommend to ouse the flag --no-dev
    


next is if you have docker installed you run in the root of your Projekt
    
    docker build -t yourtest  .
    docker run -d -e MYSQL_ROOT_PASSWORD="my-secret-pw" --name db -p 3306:3306 webtp3/tp3sql
    docker run -d  --rm -it -v $PWD:/build --link db:db -e DB_PASS="my-secret-pw" -p 80:80 --name typo3 yourtest
         
   #### start testing
   
    docker exec typo3 php vendor/phpunit/phpunit/phpunit --configuration web/typo3conf/ext/cag_tests/Tests/Build/UnitTests.xml --teamcity --log-junit 
    docker exec typo3 php vendor/phpunit/phpunit/phpunit --configuration web/typo3conf/ext/cag_tests/Tests/Build/UnitTestsDeprecated.xml --teamcity --log-junit 
    docker exec typo3 php vendor/phpunit/phpunit/phpunit --configuration web/typo3conf/ext/cag_tests/Tests/Build/FunctionalTests.xml --teamcity --log-junit 
    docker exec typo3 mkdir -p web/typo3temp/var/tests
    docker exec typo3 vendor/bin/chromedriver --url-base=/wd/hub >/dev/null 2>&1 &
    docker exec typo3 php -S 0.0.0.0:8000 >/dev/null 2>&1 &
    docker exec typo3 sleep 3;
    docker exec typo3 typo3DatabaseName='typo3' typo3DatabaseHost='DB' typo3DatabaseUsername='root' typo3DatabasePassword='my-secret-pw' vendor/codeception/codeception/codecept run Acceptance -c web/typo3conf/ext/cag_tests/Tests/Build/AcceptanceTests.yml
    
    docker stop typo3
    docker stop db
 
by changing "my-secret-pw" you change the passwords of the systems 

install typo3 via cli
       
           docker exec typo3 bash /var/www/cgi-bin/run-typo3.sh
 
Remove old Docker volumes from host

    docker volume rm $(docker volume ls -qf dangling=true) 
 
# Docker - How to cleanup (unused) resources

Once in a while, you may need to cleanup resources (containers, volumes, images, networks) ...
    
## delete volumes
    
    // see: https://github.com/chadoe/docker-cleanup-volumes
    
    $ docker volume rm $(docker volume ls -qf dangling=true)
    $ docker volume ls -qf dangling=true | xargs -r docker volume rm
    
## delete networks

    $ docker network ls  
    $ docker network ls | grep "bridge"   
    $ docker network rm $(docker network ls | grep "bridge" | awk '/ / { print $1 }')
    
## remove docker images
    
    // see: http://stackoverflow.com/questions/32723111/how-to-remove-old-and-unused-docker-images
    
    $ docker images
    $ docker rmi $(docker images --filter "dangling=true" -q --no-trunc)
    
    $ docker images | grep "none"
    $ docker rmi $(docker images | grep "none" | awk '/ / { print $3 }')

## remove docker containers

    // see: http://stackoverflow.com/questions/32723111/how-to-remove-old-and-unused-docker-images
    
    $ docker ps
    $ docker ps -a
    $ docker rm $(docker ps -qa --no-trunc --filter "status=exited")
    
## Resize disk space for docker vm
    
    $ docker-machine create --driver virtualbox --virtualbox-disk-size "40000" default
    
Documentation 
------------------

 more about the base images an be found on the docker hub:

https://hub.docker.com/r/webtp3/docker 

https://github.com/webtp3/docker

The System runs with a php fastcgi wrapper. So you can easyly swap php Versions to tests on several ons.

Web Server
https://hub.docker.com/_/ubuntu/

Database Server
https://hub.docker.com/_/mysql/

what is [tp3] https://web.tp3.de 
 
